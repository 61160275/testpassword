const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to bo true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to bo false', () => {
    expect(checkLength('1234567')).toBe(false)
  })
  test('should 25 characters to bo true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })
  test('should 26 characters to bo false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should 1 character in the password', () => {
    expect(checkDigit('1')).toBe(true)
  })

  test('should 2 character in the password', () => {
    expect(checkDigit('2')).toBe(true)
  })

  test('should 3 character in the password', () => {
    expect(checkDigit('3')).toBe(true)
  })

  test('should a character in the password', () => {
    expect(checkDigit('a')).toBe(false)
  })
})
describe('Test Symbols', () => {
  test('should ! has symbol in password to be true', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })
  test('should @ has symbol in password to be true', () => {
    expect(checkSymbol('@pakkawat')).toBe(true)
  })
  test('should ` has symbol in password to be true', () => {
    expect(checkSymbol('`J')).toBe(true)
  })

  test('should ? has symbol in password to be true', () => {
    expect(checkSymbol('11?111')).toBe(true)
  })
})

describe('Test Password', () => {
  test('should Password de@1234 to be false', () => {
    expect(checkPassword('de@1234')).toBe(false)
  })
  test('Test all password rule to be false', () => {
    expect(checkPassword('Siriphonpha275')).toBe(false)
  })
})
